import figures.*;
import materials.Color;

import java.util.ArrayList;

/**
 * class Main serves for creation figures and displays any information for task
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class Main {

    public static void main(String[] args) {

        System.out.println("=======first========");
        System.out.println("Add figure");

        PaperRectangle yellowRectangle = new PaperRectangle(1.2, 1.3);
        yellowRectangle.paint(Color.YELLOW);
        PaperRectangle redRectangle = new PaperRectangle(1.1, 1.5);
        redRectangle.paint(Color.RED);
        PaperCircle blueCircle = new PaperCircle(1.5);
        blueCircle.paint(Color.BLUE);
        PaperCircle blackCircle = new PaperCircle(2.1);
        blackCircle.paint(Color.BLACK);
        PaperTriangle whiteTriangle = new PaperTriangle(1.6);
        whiteTriangle.paint(Color.WHITE);
        PaperTriangle purpleTriangle = new PaperTriangle(1.3);
        purpleTriangle.paint(Color.PURPLE);
        PaperTriangle azureTriangle = new PaperTriangle(0.9);
        azureTriangle.paint(Color.AZURE);
        FilmTriangle filmTriangle = new FilmTriangle(0.9);
        FilmCircle filmCircle = new FilmCircle(0.5);
        FilmRectangle filmRectangle = new FilmRectangle(0.7, 0.6);

        System.out.println("======second=====");
        System.out.println("Put all figures in the BOX");

        System.out.println("1. Create singleton BoxFigures");
        BoxFigures boxFigures = BoxFigures.getInstance();
        System.out.println("2. Put first figure in the BOX");
        boxFigures.createBox(yellowRectangle);
        System.out.println("3. Add figures");
        boxFigures.addFigures(redRectangle);
        boxFigures.addFigures(blueCircle);
        boxFigures.addFigures(blackCircle);
        boxFigures.addFigures(whiteTriangle);
        boxFigures.addFigures(purpleTriangle);
        boxFigures.addFigures(azureTriangle);
        boxFigures.addFigures(filmCircle);
        boxFigures.addFigures(filmRectangle);
        boxFigures.addFigures(filmTriangle);
        System.out.println("4. All figures:" + "\n");
        System.out.println(boxFigures.toString());

        System.out.println("=====third======");
        System.out.println("Check all methods of the BOX");
        System.out.println("1. Show figure by number: ");
        int numFigure = 3;
        System.out.println(boxFigures.showFigureNum(numFigure) + "\n");

        System.out.println("2. Extraction of figure: ");
        boxFigures.extractFigure(numFigure);
        System.out.println("\n" + boxFigures.toString());

        System.out.println("3. Replacing of figure by number: ");
        int numReplace = 4;
        PaperCircle purpleCircle = new PaperCircle(2.1);
        purpleCircle.paint(Color.PURPLE);
        boxFigures.replaceFigure(numReplace, purpleCircle);
        System.out.println("\n" + boxFigures.toString());

        System.out.println("4. Find all similar figures: " + "\n");
        PaperCircle redCircle = new PaperCircle(2.05);
        redCircle.paint(Color.BLACK);
        ArrayList<Figure> allFindFigures = boxFigures.findFigure(redCircle);
        for (Figure figure : allFindFigures) {
            System.out.println(figure);
        }

        System.out.println("5. Number of figures in the BOX: " + "\n");
        System.out.println(boxFigures.countFigures());

        System.out.println("\n" + "6. Sum of the area all figures in the BOX: " + "\n");
        System.out.println(boxFigures.sumAreaFigures());

        System.out.println("\n" + "7. Sum of the perimeter all figures in the BOX: " + "\n");
        System.out.println(boxFigures.sumPerimeterFigures());

        System.out.println("\n" + "8. Get all Circles in the BOX: " + "\n");
        ArrayList<Figure> circles = boxFigures.getAllCircle();

        for (Figure figure : circles) {
            System.out.println(figure);
        }

        System.out.println("\n" + "9. Get all Film Figures in the BOX: " + "\n");
        ArrayList<Figure> filmFigures = boxFigures.getAllFilmFigures();

        for (Figure figure : filmFigures) {
            System.out.println(figure);
        }

        PaperCircle paperCircle = new PaperCircle(0.3);
        boxFigures.cutFigure(1, paperCircle);

        System.out.println(boxFigures.toString());

        PaperTriangle paperTriangle = new PaperTriangle(1.1);
        boxFigures.cutFigure(2, paperTriangle);

        System.out.println(boxFigures.toString());


    }
}
