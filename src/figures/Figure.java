package figures;

/**
 * It's abstract class Figure
 * @author Nikolay Sizykh
 * @version 1.1
 */

public abstract class Figure {

    /**
     * This is abstract method. It's wil be overridden for each descendant classes
     * @return area of a figure
     */
    public abstract double getArea();

    /**
     * This is abstract method. It's wil be overridden for each descendant classes
     * @return perimeter of a figure
     */
    public abstract double getPerimeter();

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
