package figures;

import java.util.Objects;

/**
 * Abstract class for Rectangle
 * @author Nikolay Sizykh
 * @version 1.1
 */

public abstract class Rectangle extends Figure {

    /**
     * first side of Rectangle
     */
    double sideA;
    /**
     * second side of Rectangle
     */
    double sideB;


    public Rectangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public Rectangle() {
    }

    /**
     * this constructor for cutting rectangles
     * @param figure a figure for cutting
     * @param difArea a difference between areas of existing figure and new figure
     */
    public Rectangle(Figure figure, double difArea) {
        this.sideA = Math.sqrt(figure.getArea() - difArea);
        this.sideB = this.sideA;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    @Override
    public double getArea() {
        return sideA * sideB;
    }

    @Override
    public double getPerimeter() {
        return 2 * (sideB + sideA);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle)) return false;
        if (!super.equals(o)) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.sideA, sideA) == 0 &&
                Double.compare(rectangle.sideB, sideB) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sideA, sideB);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "sideA=" + sideA +
                ", sideB=" + sideB +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                '}';
    }
}
