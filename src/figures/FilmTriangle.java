package figures;

import materials.IFilm;

/**
 * class Film Triangle which extends abstract class Triangle and implements interface IFilm
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class FilmTriangle extends Triangle implements IFilm {

    public FilmTriangle(double side) {
        super(side);
    }

    public FilmTriangle(Figure figure, double difArea) {
        super(figure, difArea);
    }

    public FilmTriangle() {
    }

    @Override
    public String toString() {
        return "FilmTriangle{" +
                "side=" + side +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +

                '}' + "\n";
    }
}
