package figures;

import materials.IFilm;
/**
 * class Film Rectangle which extends abstract class Rectangle and implements interface IFilm
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class FilmRectangle extends Rectangle implements IFilm {
    public FilmRectangle(double sideA, double sideB) {
        super(sideA, sideB);
    }

    public FilmRectangle(Figure figure, double difArea) {
        super(figure, difArea);
    }

    public FilmRectangle() {
    }



    @Override
    public String toString() {
        return "FilmRectangle{" +
                "sideA = " + sideA +
                ", sideB = " + sideB +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                '}' + "\n";
    }
}
