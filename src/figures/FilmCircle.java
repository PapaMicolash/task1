package figures;

import materials.IFilm;

/**
 * class Film triangle which extends abstract class Triangle and implements interface IFilm
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class FilmCircle extends Circle implements IFilm {

    public FilmCircle(double radius) {
        super(radius);
    }

    public FilmCircle(Figure figure, double difArea) {
        super(figure, difArea);
    }

    public FilmCircle() {
    }

    @Override
    public String toString() {
        return "FilmCircle{" +
                "radius = " + radius +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                '}' + "\n";
    }
}
