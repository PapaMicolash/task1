package figures;

import materials.Color;
import materials.IPaper;

import java.util.Objects;

/**
 * class Paper triangle which extends abstract class Triangle and implements interface IPaper
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class PaperTriangle extends Triangle implements IPaper {


    /**
     * a color of triangle
     */
    Color color = null;

    public PaperTriangle(double side, Color color) {
        super(side);
        this.color = color;
    }

    public PaperTriangle(double side) {
        super(side);
    }


    public PaperTriangle(Figure figure, double difArea) {
        super(figure, difArea);
        if (figure instanceof IPaper) {
            this.color = ((IPaper)figure).getColor();
        }
    }

    /**
     * this is boolean method which check and paint figure if figure without color or dont paint if figure with color
     * @param color this is a part of enum class Color
     * @return true or false
     */
    @Override
    public boolean paint(Color color) {
        if (this.color == null) {
            this.color = color;
            return true;
        }
        else return false;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaperTriangle)) return false;
        if (!super.equals(o)) return false;
        PaperTriangle that = (PaperTriangle) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    @Override
    public String toString() {
        return "PaperTriangle{" +
                "color=" + color +
                ", side=" + side +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +

                '}'+ "\n";
    }
}
